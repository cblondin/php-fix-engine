<?php
require_once __DIR__ . '/../../php/.php';

if (!$socket = fsockopen('127.0.0.1', 8080)) {
    trigger_error('failed to open socket');
}

print 'request: ';

if (!fwrite($socket, trim(fgets(STDIN)))) {
    trigger_error('failed to send request');
}

if (!is_string($response = fread($socket, 1024))) {
    trigger_error('failed to get response');
}

print "$response\n";

if (!fclose($socket)) {
    trigger_error('failed to close socket');
}
?>
